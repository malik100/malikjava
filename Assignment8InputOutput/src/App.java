import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) throws IOException {
		Address add1 = new Address("100", "Up street", "Montreal", "Quebec", "1a2b3c");
		Address add2 = new Address("500", "Down street", "Laval", "Quebec", "4d5e6f");
		Address add3 = new Address("2000", "Old street", "Paris", "Quebec", "7g8h9i");
		Address add4 = new Address("333", "New street", "London", "Quebec", "3j3k3l");
		Address add5 = new Address("101", "Big street", "Toronto", "Quebec", "5m5n5o");
		List<Address> addressList = new ArrayList<Address>();
		addressList.add(add1);
		addressList.add(add2);
		addressList.add(add3);
		addressList.add(add4);
		addressList.add(add5);
		
		writeToFile(addressList);
		readFromFile(addressList);
	}

	public static String writeToFile(List<Address> addressList) throws IOException {
		//SETTING UP OUTPUT PORTION OF CODE
				FileWriter fileWriter = new FileWriter("resource//output.txt");
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				String message = "hello";
				//LOOPS AND OUTPUTS THE .TOSTRING() OF EACH CLASS TO A NEW LINE
				for (int i = 0; i < addressList.size(); i++) {
					bufferedWriter.write(addressList.get(i).toString() + "\n");
				}
				
				bufferedWriter.close();
				fileWriter.close();
		return "";
	}
	
	public static String readFromFile(List<Address> addressList) throws IOException {
		//THE READER PORTION OF THE CODE
				FileReader fileReader = new FileReader("resource//output.txt");
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				String address = bufferedReader.readLine();
				while(address != null) {
				System.out.println(address);
				address = bufferedReader.readLine();
				}
				bufferedReader.close();
				fileReader.close();
		return "";
	}
	
}
