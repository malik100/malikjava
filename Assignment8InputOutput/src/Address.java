
public class Address {
	private String buildingNo;
	private String streetName;
	private String cityName;
	private String provinceName;
	private String postalCode;
	public Address(String buildingNo, String streetName, String cityName, String provineName, String postalCode) {
		this.buildingNo = buildingNo;
		this.streetName = streetName;
		this.cityName = cityName;
		this.provinceName = provineName;
		this.postalCode = postalCode;
	}
	public String getBuildingNo() {
		return buildingNo;
	}
	public String getStreetName() {
		return streetName;
	}
	public String getCityName() {
		return cityName;
	}
	public String getProvineName() {
		return provinceName;
	}
	public String getPostalCode() {
		return postalCode;
	}
	
	@Override
	public String toString() {
		return this.buildingNo + " " + this.streetName + " " + this.cityName + 
				" " + this.provinceName + " " + this.postalCode;
	}
}
