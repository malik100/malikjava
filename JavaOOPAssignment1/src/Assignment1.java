//by: Malik Zafar
class Person{
	String firstName;
	String lastName;
	int age;
	
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public int getAge() {
		return age;
	}
	public void setFirstName(String x) {
		firstName = x;
	}
	public void setLastName(String y) {
		lastName = y;
	}
	public void setAge(int z) {
		if(z < 0 || z > 100) {
			age = 0;
		}else {
			age = z;
		}
	}
	public boolean isTeen() {
		if(age > 12 && age < 20) {
			return true;
		}
		return false;
	}
	public String getFullName() {
		String fullName = "";
		if (firstName.isEmpty() && lastName.isEmpty()) {
			return fullName;
		}else if(lastName.isEmpty()) {
			return firstName;
		}else if(firstName.isEmpty()){
			return lastName;
		}else {
			return fullName = firstName + " " + lastName;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////

class SimpleCalculator{
	double firstNumber;
	double secondNumber;
	
	public double getFirstNumber() {
		return firstNumber;
	}
	public double getSecondNumber() {
		return secondNumber;
	}
	public void setFirstNumber(double x) {
		firstNumber = x;
	}
	public void setSecondNumber(double y) {
		secondNumber = y;
	}
	public double getAdditionResult() {
		return (firstNumber + secondNumber);
	}
	public double getSubtractionResult() {
		return (firstNumber - secondNumber);
	}
	public double getMultiplicationResult() {
		return (firstNumber * secondNumber);
	}
	public double getDivisionResult() {
		if(secondNumber == 0){
			return secondNumber;
		}
		return (firstNumber / secondNumber);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////

class Wall{
	double width;
	double height;
	
	public Wall(){
		
	}
	public Wall(double width, double height) {
		if(width < 0) {
			this.width = 0;
		}else {
			this.width = width;
		}
		if(height < 0) {
			this.height = 0;
		}else {
			this.height = height;
		}
	}
	public double getWidth() {
		return width;
	}
	public double getHeight() {
		return height;
	}
	public void setWidth(double x) {
		if(x < 0) {
			width = 0;
		}else {
			width = x;
		}
	}
	public void setHeight(double y) {
		if(y < 0) {
			height = 0;
		}else {
			height = y;
		}
	}
	public double getArea() {
		return (width * height);
	}
}
public class Assignment1 {

	public static void main(String[] args) {
		///TEST OUTPUTS FOR PERSON CLASS
		Person person = new Person();
		person.setFirstName("");
		person.setLastName("");
		person.setAge(10);
		System.out.println("fullName= " + person.getFullName());
		System.out.println("teen= " + person.isTeen());
		person.setFirstName("John");
		person.setAge(18);
		System.out.println("fullName= " + person.getFullName());
		System.out.println("teen= " + person.isTeen());
		person.setLastName("Smith");
		System.out.println("fullName= " + person.getFullName());
		System.out.println("------------------");
		
		//TEST OUTPUTS OF SIMPLE CALCULATOR CLASS
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(4);
		System.out.println("add= " + calculator.getAdditionResult());
		System.out.println("Subtract= " + calculator.getSubtractionResult());
		calculator.setFirstNumber(5.25);
		calculator.setSecondNumber(0);
		System.out.println("multiply= " + calculator.getMultiplicationResult());
		System.out.println("divide= " + calculator.getDivisionResult());
		System.out.println("------------------");
		
		//////TEST OUTPUTS FOR WALL CLASS
		Wall wall = new Wall(5,4);
		System.out.println("area= " + wall.getArea());
		wall.setHeight(-1.5);
		System.out.println("width= " + wall.getWidth());
		System.out.println("height= " + wall.getHeight());
		System.out.println("area= " + wall.getArea());
	}

}
