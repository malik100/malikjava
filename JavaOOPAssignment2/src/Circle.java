
public class Circle extends Shape {
	double length;// (diameter)

	@Override
	double getArea(double length) {
		double area = 3.14 * Math.pow(length / 2, 2);
		return area;
	}

	@Override
	double getPerimeter(double length) {
		double circumference = 2 * 3.14 * (length / 2);
		return circumference;
	}
}
