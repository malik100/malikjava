
public class Square extends Shape{
	double length;
	@Override
	double getArea(double length) {
		return (length * length);
	}
	@Override
	double getPerimeter(double length) {
		return (length * 4);
	}
}
