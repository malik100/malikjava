//STUDENT: MALIK ZAFAR
///////THIS FILE HAS THE OUTPUTS OF THE CLASSES AND THEIR METHODS IN THIS ASSIGNMENT

//THE DIFFERENCE BETWEEN USING AND NOT USING ABSTRACT WAS THAT I HAD TO COPY PASTE SIMILAR CODE FOR
//EACH CLASS AND CHANGE SMALL DETAILS IN EACH CLASS BUT WHEN I USED ABSTRACT I JUST HAD TO CHANGE 
//A FEW MINOR THINGS FOR THE CODE TO WORK WITHOUT WORRYING ABOUT RECREATING EACH CLASS COMPLETELY.
public class Assignment2OUTPUTS {

	public static void main(String[] args) {
		Circle circle = new Circle();
		circle.length = 10;
		System.out.println("circumference of the circle = " + circle.getPerimeter(10));
		System.out.println("area of the circle = " + circle.getArea(10));
		
		Square square = new Square();
		square.length = 10;
		System.out.println("perimeter of the square = " +square.getPerimeter(10));
		System.out.println("area of the square = " + square.getArea(10));
		
		Cats cat = new Cats();
		cat.getSound();
		
		Dogs dog = new Dogs();
		dog.getSound();
		
		A studentA = new A(100, 75, 55, 60);
		System.out.println("studentA percentage is " +studentA.getPercentage());
		
		B studentB = new B(80, 40, 77);
		System.out.println("studentB percentage is " + studentB.getPercentage());
	}

}
