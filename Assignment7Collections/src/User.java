import java.time.LocalDate;
import java.util.List;

public class User implements Comparable<User>{
	private int id;
	private String name;
	private java.time.LocalDate birthdate;
	
	public User(int id, String name, LocalDate birthdate) {
		this.id = id;
		this.name = name;
		this.birthdate = birthdate;
	}	

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public java.time.LocalDate getBirthdate() {
		return birthdate;
	}
	
	@Override
	public int compareTo(User o) {
		return this.name.compareTo(o.name);
		
	}
	
	public int compareTo(int i, User o) {
			
			if(this.id == o.id) {
				return 0;
			}else if(this.id > o.id) {
				return 1;
			}else {
				return -1;
			}
		
	}
	@Override
	public String toString() {
		return "name= " + this.name + " id= " + this.id + " birthday= " + this.birthdate;
	}
}
