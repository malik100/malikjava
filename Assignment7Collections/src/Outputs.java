import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Outputs {

	public static void main(String[] args) {

		LocalDate date1 = LocalDate.of(2000, 1, 1);
		LocalDate date2 = LocalDate.of(1980, 2, 2);
		LocalDate date3 = LocalDate.of(1990, 5, 5);
		LocalDate date4 = LocalDate.of(1970, 9, 9);
		LocalDate date5 = LocalDate.of(2010, 10, 10);

		User user1 = new User(100, "Lucy", date1);
		User user2 = new User(700, "Kevin", date2);
		User user3 = new User(123, "Mark", date3);
		User user4 = new User(555, "Jenny", date4);
		User user5 = new User(1000, "Joe", date5);

		List<User> userList = new ArrayList<User>();

		userList.add(user1);
		userList.add(user2);
		userList.add(user3);
		userList.add(user4);
		userList.add(user5);
		System.out.println("before sort");
		for (User u : userList) {
			System.out.println(u);
		}

//		Collections.sort(userList);
		System.out.println();
		System.out.println("after sort");
		sortListByType("name", userList); //sorts by type name
		System.out.println();
//		sortListByType(10, userList);
		
	}

	public static void sortListByType(String name, List<User> userList) {
		Collections.sort(userList);
		for (User u : userList) {
			System.out.println(u);
		}
	}
//	public static void sortListByType(int id, List<User> userList) {
//		Collections.compareTo(10 ,userList);
//		for (User u : userList) {
//			System.out.println(u);
//		}
//	}

}
