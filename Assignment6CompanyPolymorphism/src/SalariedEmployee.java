
public class SalariedEmployee extends Employee{
	private double basicSalary;
	
	public SalariedEmployee(String name, String ssn, double basicSalary) {
		super(name, ssn);
		this.basicSalary = basicSalary;
	}
	
	@Override
	public double salary() {
		return basicSalary;
	}
	
	//.salary() is inherited unchanged
}
