
public abstract class Employee {
	private String name;
	private String ssn;
	

	public Employee(String name, String ssn) {
		this.name = name;
		this.ssn = ssn;
	}

	public double salary() {
		return 0.0;
	}
	
	public String toString() {
		return "name = " + this.name + " salary = " + this.salary();
	}
	@Override
	public int hashCode() {
		return Integer.parseInt(this.ssn);
	}
	//ssn not included in the toString method to hide ssn
	public String getSSN() {
		return this.ssn;
	}
}
