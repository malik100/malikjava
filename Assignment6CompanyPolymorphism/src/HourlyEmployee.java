
public class HourlyEmployee extends Employee{
	private double wage;
	private double hours;
	
	public HourlyEmployee(String name, String ssn, double wage, double hours) {
		super(name, ssn);
		this.wage = wage;
		this.hours = hours;
	}
	
	@Override
	public double salary() {
		return wage * hours;
	}
	
	//.salary() is inherited unchanged
}
