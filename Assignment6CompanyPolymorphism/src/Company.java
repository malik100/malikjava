import java.util.ArrayList;

public class Company {
	public static void main(String[] args) {
		
		Employee employee1 = new CommissionEmployee("Jake", "123456", 100.0, 5.0 );
		Employee employee2 = new HourlyEmployee("Mary", "098765", 15.0, 30.0 );
		Employee employee3 = new SalariedEmployee("Victor", "111111", 1500.0 );
		
		Payroll payroll = new Payroll();
		payroll.setEmployee(employee1);
		payroll.setEmployee(employee2);
		payroll.setEmployee(employee3);
		
		for (int i = 0; i < payroll.getEmployee().size(); i++) {
			Employee em = payroll.getEmployee(i);
			payroll.paySalary(em);
		}
		
		
	}
}
