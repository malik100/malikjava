package EmailAndSMS;

public class SmsUser extends User{
	private String phoneNumber; //used string instead of int to be able to run validation on it

	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		for (int i = 0; i < phoneNumber.length(); i++) { //using ascii table to check if all characters are numbers
			if(phoneNumber.charAt(i) < 48 || phoneNumber.charAt(i) > 57) {
				throw new IllegalArgumentException();
			}
		}
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
