package EmailAndSMS;

import java.io.IOException;

public interface ISendInfo {
	boolean validateMessage(User sender, User reciever, String body);
	
	void sendMessage(Message message) throws IOException;

}
