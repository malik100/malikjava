
public class Output {

	public static void main(String[] args) {
		///Creating Dancers
		Dancer dancer = new Dancer();
		dancer.setAge(50);
		dancer.setName("Carl");
		Dancer electricBoogieDancer = new ElectricBoogieDancer();
		electricBoogieDancer.setAge(25);
		electricBoogieDancer.setName("Suzy");
		Dancer breakdancer = new Breakdancer();
		breakdancer.setAge(20);
		breakdancer.setName("Mike");
		//Putting dancers in array
		Dancer[] danceTeam = {dancer, electricBoogieDancer, breakdancer};
		
		//Array loops thorugh all dancers and displays their Overridden dance method
		for (int i = 0; i < danceTeam.length; i++) {
			Dancer d = danceTeam[i];
			System.out.print(d.getName() + " who is " + d.getAge() + " ");
			d.dance();
		}
	}

}
