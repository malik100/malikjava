
public class Dancer {
	private int age;
	private String name;
	
	public void dance() {
		System.out.println("did standand dancing!");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
