
public class Resolution {
	private double verticalPixels;
	private double horizontalPixels;
	
	///2 FIELD CONSTRUCTOR
	public Resolution(double verticalPixels, double horizontalPixels) { 
		this.verticalPixels = verticalPixels;
		this.horizontalPixels = horizontalPixels;
	}
	

/////ALL GETTERS AND SETTEERS FOR THIS CLASS
	public double getVerticalPixels() {
		return verticalPixels;
	}

	public void setVerticalPixels(double verticalPixels) {
		this.verticalPixels = verticalPixels;
	}

	public double getHorizontalPixels() {
		return horizontalPixels;
	}

	public void setHorizontalPixels(double horizontalPixels) {
		this.horizontalPixels = horizontalPixels;
	}
	
	
}
