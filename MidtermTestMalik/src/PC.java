
public class PC {
	private Case theCase;
	private Monitor theMonitor;
	private Motherboard theMotherboard;
	
	///3 FIELD CONSTRUCTOR
	public PC(Case theCase, Monitor theMonitor, Motherboard theMotherboard) {
		this.theCase = theCase;
		this.theMonitor = theMonitor;
		this.theMotherboard = theMotherboard;
	}
	
	public void startupSequence() {
		System.out.println("Loading components");
		System.out.println("Verifying Case");
		System.out.println("Case model is: " + theCase.getModel());
		System.out.println("Case manufacturer is: " + theCase.getManufacturer());
		System.out.println("Case power supply is: " + theCase.getPowersupply());
		System.out.println("Case dimensions are : ");
		System.out.println("width : " + theCase.dimensions.getWidth());
		System.out.println("height : " + theCase.dimensions.getHeight());
		System.out.println("depth : " + theCase.dimensions.getDepth());
		System.out.println("Verifying Monitor");
		System.out.println("Monitor model is: " + theMonitor.getModel());
		System.out.println("Monitor manufacturer is: " + theMonitor.getManufacturer());
		System.out.println("Monitor power size is: " + theMonitor.getSize());
		System.out.println("Monitor resolution is: " + theMonitor.resolution.getVerticalPixels() + 
				" X " + theMonitor.resolution.getHorizontalPixels());
		System.out.println("Verifying Motherboard");
		System.out.println("Motherboard model is: " + theMotherboard.getModel());
		System.out.println("Motherboard manufacturer is: " + theMotherboard.getManufacturer());
		System.out.println("Motherboard has: " + theMotherboard.getRamslots() + " ramslots");
		System.out.println("Motherboard has: " + theMotherboard.getCardslots()+ " cardslots");
		System.out.println("Bios version is: " + theMotherboard.getBios());
		System.out.println("All systems verified loading Desktop");
	}
////GETTERS AND SETTERS
	public Case getTheCase() {
		return theCase;
	}

	public void setTheCase(Case theCase) {
		this.theCase = theCase;
	}

	public Monitor getTheMonitor() {
		return theMonitor;
	}

	public void setTheMonitor(Monitor theMonitor) {
		this.theMonitor = theMonitor;
	}

	public Motherboard getTheMotherboard() {
		return theMotherboard;
	}

	public void setTheMotherboard(Motherboard theMotherboard) {
		this.theMotherboard = theMotherboard;
	}
	
	
}
