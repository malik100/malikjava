
public class Monitor {
	private String model;
	private String manufacturer;
	private double size;
	Resolution resolution;
	
	public Monitor(String model, String manufacturer, double size, Resolution resolution) {
		this.model = model;
		this.manufacturer = manufacturer;
		this.size = size;
		this.resolution = resolution;
	}
	
	public void turnOnScreen() {
		System.out.println("The screen is truned on");
	}
	//////////ALL GETTERS AND SETTERS
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public Resolution getResolution() {
		return resolution;
	}
	public void setResolution(Resolution resolution) {
		this.resolution = resolution;
	}
	
	
	
}
